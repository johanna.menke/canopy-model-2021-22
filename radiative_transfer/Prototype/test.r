# test
meteo <- read.csv("Measurements_meteo_hourly_201601_201712_gapfilled.csv")
fluxes <- read.csv("Measurements_fluxes_hourly_201601_201712_gapfilled.csv")
soil <- read.csv("Measurements_soil_hourly_201601_201712_gapfilled.csv")
parameters <- read.csv("parameters.csv")

#needed inputs:
# * incoming solar (diffuse/direct)
# * incoming longwave

# solar:

sw_dir <- meteo$SW_IN_Wm.2 - meteo$SW_DIF_IN_Wm.2
sw_dif <- meteo$SW_DIF_IN_Wm.2 
sw_total <- meteo$SW_IN_Wm.2


# longwave:

long_in <- meteo$LW_IN_Wm.2

# assumption for LAI:
LAI <- 5.1
# assumption for Extinction Coefficient Kb:
Kb <- 1 #Bonan 2015: assumption: horizontal leaf orientation

# from table 14.1 p.231:
transmittance <- 0.05
reflectance <- 0.1

# Transmitted short wave (direct) eq. 14.35 p. 241:
## ΔL fraction of radiation that is intercepted = e^-KbL
deltaL <- exp(-Kb*LAI)
#I_trans = sw_total*((1-deltaL)+transmittance*deltaL)
I_trans_dir = sw_dir*(1-deltaL)
I_trans_dif = sw_total*transmittance*deltaL
I_trans_total = I_trans_dir + I_trans_dif
plot(as.factor(na.omit(meteo$TIMESTAMP_END)), I_trans)







rad_diff <- meteo$SW_IN_Wm.2 - meteo$SW_OUT_Wm.2
check <- rad_diff - I_trans_total



