### Package ----
library(purrr) # get_LAI_layers contain map in purrr package
library(tidyverse) # to use tibble
library(lubridate)
library(here) # to deal with annoying file paths


### Load the parameters ----
# source(here::here("utility/setup_parameters.R"))
pars <- read.csv("Measurements/parameters.csv", header = TRUE, row.names = 1) # global parameters
pars$value[pars$variable == "p"] <- 10
pars$value[pars$variable == "q"] <- 4
# Change beta function more asymmetric

## Constant ## ----
sigma <- 5.67e-08 # Stefan-Boltzmann constant
T_0 <- 273.15 # Freezing point of water (K)

## Input ## ----

## Canopy
N_veg_layers <- 2 # Number of canopy layers
# Probably if we slice our canopy into pieces, the outgoing radiation will be underestimated.
LAI <- 5.1 # Leaf Area Index - Entire Canopy
# winter: tree stems, branches -> really small
# max_LAI,5.1

Omega <- 0.7 # Clumping Index

## Leaf
# T_l data from air temperature 
K_b <- 1 # Direct beam extinction coefficient

# Parameters for shortwave
omega_l_I <- 0.5 # leaf scattering coefficient. The scattering is then split into leaf transmittance and leaf reflectance (Page 256)
rho_l_I <- omega_l_I / 2 # In this case half of the scattering is reflected
tau_l_I <- omega_l_I - rho_l_I # The other half of the scattering is transmitted

# Parameters for longwave
eps_l <- 0.98 # emissivity leaf
omega_l_L <- 1 - eps_l # leaf scattering coefficient
rho_l_L <- omega_l_L # In this case all the scattering is reflected
tau_l_L <- omega_l_L - rho_l_L # This will be 0

## Ground
# T_g <- T_0 + 20 # Ground temperature (K)
# we can use 2 cm data from the measurement file
rho_gd <- 0.1 # diffuse ground albedo - reflectance of ground
rho_gb <- 0.1 # direct beam ground albedo - reflectance of ground
eps_g <- 1 # emissivity ground
rho_g <- 1 - eps_g # reflectance of ground


## Radiation, ground temperature, leaf (air) temperature
input_meteo <- c("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)
input_soil <- c("Measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)
input_gap <- input_meteo %>% 
  inner_join(input_soil, by = "TIMESTAMP_END") 
names(input_gap)


input_test <- input_gap %>% 
  filter(month(TIMESTAMP_END) == "7" & year(TIMESTAMP_END) == "2016") %>%  # July 2016
  select(Timestamp = "TIMESTAMP_END",
         SW_Inc_b = "SW_OUT_Wm-2",
         SW_Inc_d = "SW_DIF_IN_Wm-2",
         LW_Inc = "LW_IN_Wm-2",
         T_g = "TS_2cm_degC",
         T_l = "TA_degC",
         SW_out = "SW_OUT_Wm-2",
         LW_out = "LW_OUT_Wm-2")


##  Timestep
timestep <- length(input_test$Timestamp)


### Functions ## ----


norman_model <- function(SW_Inc_b, SW_Inc_d, LW_Inc, T_g, T_l) {
  

  # canopy profile function
  canopy_LAI_profile <- function(x) {
    (x^(pars$value[pars$variable == "p"] - 1) * (1 - x)^(pars$value[pars$variable == "q"] - 1)) /
      beta(pars$value[pars$variable == "p"], pars$value[pars$variable == "q"])
  }

  # function for delta LAI
  get_LAI_layers <- function(LAI, n_layers) { # calculate delta LAI by layer

    # divide the vertical profile to have the upper and lower points for n layers
    breaks <- seq(0, 1, length.out = n_layers + 1)
    lower_limit <- breaks[1:length(breaks) - 1]
    upper_limit <- breaks[2:length(breaks)]

    # for each of the layer `integrate` `canopy_LAI_profile` with the given limits
    LAI_layers <- map2(lower_limit, upper_limit, integrate, f = canopy_LAI_profile) %>%
      map(`[[`, "value") %>% # extract only the `value` from the integral result
      as.double() # simplify list

    LAI_layers <- LAI_layers * LAI # scale to actual LAI

    tibble(n_layer = seq(n_layers), LAI = LAI_layers)
  }

  # function for tridiagonal matrix
  solve_tridiagonal <- function(a, b, c, d, n) {
    e <- numeric(n - 1)
    f <- numeric(n)
    u <- numeric(n)

    e[1] <- c[1] / b[1]

    for (i in 2:(n - 1)) {
      e[i] <- c[i] / (b[i] - a[i] * e[i - 1])
    }
    f[1] <- d[1] / b[1]

    for (i in 2:n) {
      f[i] <- (d[i] - a[i] * f[i - 1]) / (b[i] - a[i] * e[i - 1])
    }
    u[n] <- f[n]

    for (i in (n - 1):1) {
      u[i] <- f[i] - e[i] * u[i + 1]
    }
    return(u)
  }


  # Define canopy layers ----
  N_layers <- N_veg_layers + 1 # Total number of layers
  i_soil <- 1 # Index of the soil layer
  i_bot_l <- i_soil + 1 # Index for bottom leaf layer
  i_top_l <- N_veg_layers + i_bot_l - 1 # Index for top leaf layer

  # Calculate LAI by layer ----
  LAI_layer <- get_LAI_layers(LAI, N_veg_layers)
  d_LAI <- c(0, LAI_layer$LAI) # LAI[1] = soil = 0
  length(d_LAI) # 35 + 1
  
  # For for loop
  I_g <- numeric()
  I_c <- numeric()
  I_top <- numeric()
  L_g <- numeric()
  L_c <- numeric()
  L_top <- numeric()
  
  for(k in 1: timestep){
    
    ### Shortwave Radiation ----
    
    x <- numeric() # Cumulative Leaf Area Index
    T_b <- numeric() # Fraction of un-intercepted direct beam radiation
    
    for (i in i_soil:i_top_l) {
      x[i] <- sum(d_LAI[(i + 1):i_top_l])
      T_b[i] <- exp(-K_b * Omega * x[i])
    }
    x[i_top_l] <- 0
    T_b[i_top_l] <- 1
    
    tau_d <- numeric() # diffuse transmittance for horizontal leaf
    tau_b <- numeric() # direct beam transmittance for horizontal leaf
    
    for (i in i_soil:i_top_l) {
      tau_d[i] <- exp(-Omega * d_LAI[i])
      tau_b[i] <- exp(-K_b * Omega * d_LAI[i])
    }
    
    ### Calculate Tridiagonal matrix values ----
    x_tri <- numeric(2 * N_layers) # Values below the diagonal
    y_tri <- numeric(2 * N_layers) # Values on the diagonal
    z_tri <- numeric(2 * N_layers) # Values above the diagonal
    w_tri <- numeric(2 * N_layers) # Values on the right side of the equation
    
    ### Calculations for the Soil Layer
    i <- i_soil
    
    # Soil Upward Flux - First row of the 14.42 Matrix
    x_tri[2 * i - 1] <- 0
    y_tri[2 * i - 1] <- 1
    z_tri[2 * i - 1] <- -rho_gd
    w_tri[2 * i - 1] <- rho_gd * SW_Inc_b[k] * T_b[i] # c0 from Eq. (14.45)
    
    # Soil Downward Flux - Second row of the 14.42 Matrix
    reflected <- (1 - tau_d[i + 1]) * rho_l_I # left part of equation (14.43)
    transmitted <- (1 - tau_d[i + 1]) * tau_l_I + tau_d[i + 1] # numerator of fraction in right part of equation (14.43)
    a_i <- reflected - (transmitted^2) / reflected # Equation (14.43)
    b_i <- transmitted / reflected # Equation (14.44)
    d_i <- SW_Inc_b[k] * T_b[i + 1] * (1 - tau_b[i + 1]) * (tau_l_I - rho_l_I * b_i) # Equation (14.46)
    
    x_tri[2 * i + 0] <- -a_i
    y_tri[2 * i + 0] <- 1
    z_tri[2 * i + 0] <- -b_i
    w_tri[2 * i + 0] <- d_i
    
    
    ### Calculations for the Canopy Layers
    
    for (i in i_bot_l:(i_top_l - 1)) {
      
      # Canopy Layer Upward flux
      
      reflected <- (1 - tau_d[i]) * rho_l_I
      transmitted <- (1 - tau_d[i]) * tau_l_I + tau_d[i]
      f_i <- reflected - (transmitted^2) / reflected # Equation (14.43), same as a_{i-1}
      e_i <- transmitted / reflected # Equation (14.44), same as b_{i-1}
      c_i <- SW_Inc_b[k] * T_b[i] * (1 - tau_b[i]) * (rho_l_I - tau_l_I * e_i) # Equation (14.45)
      
      x_tri[2 * i - 1] <- -e_i
      y_tri[2 * i - 1] <- 1
      z_tri[2 * i - 1] <- -f_i
      w_tri[2 * i - 1] <- c_i
      
      # Canopy Layer Downward flux
      
      reflected <- (1 - tau_d[i + 1]) * rho_l_I
      transmitted <- (1 - tau_d[i + 1]) * tau_l_I + tau_d[i + 1]
      a_i <- reflected - (transmitted^2) / reflected # Equation (14.43)
      b_i <- transmitted / reflected # Equation (14.44)
      d_i <- SW_Inc_b[k] * T_b[i + 1] * (1 - tau_b[i + 1]) * (tau_l_I - rho_l_I * b_i) # Equation (14.46)
      
      x_tri[2 * i + 0] <- -a_i
      y_tri[2 * i + 0] <- 1
      z_tri[2 * i + 0] <- -b_i
      w_tri[2 * i + 0] <- d_i
    }
    
    ### Calculations for the top Canopy Layer
    i <- i_top_l
    
    # Top Canopy Layer Upward flux - Second to last row of the 14.42 Matrix
    
    reflected <- (1 - tau_d[i]) * rho_l_I
    transmitted <- (1 - tau_d[i]) * tau_l_I + tau_d[i]
    f_i <- reflected - (transmitted^2) / reflected # Equation (14.43), same as a_{i-1}
    e_i <- transmitted / reflected # Equation (14.44), same as b_{i-1}
    c_i <- SW_Inc_b[k] * T_b[i] * (1 - tau_b[i]) * (rho_l_I - tau_l_I * e_i) # Equation (14.45)
    
    x_tri[2 * i - 1] <- -e_i
    y_tri[2 * i - 1] <- 1
    z_tri[2 * i - 1] <- -f_i
    w_tri[2 * i - 1] <- c_i
    
    # Top  Canopy Layer Downward flux - Last row of the 14.42 Matrix
    
    x_tri[2 * i + 0] <- 0
    y_tri[2 * i + 0] <- 1
    z_tri[2 * i + 0] <- 0
    w_tri[2 * i + 0] <- SW_Inc_d[k] # dN from Eq. (14.46)
    
    # u_tri <- tridiagmatrix(x_tri[-1], y_tri, z_tri[-72], w_tri) # function from cmna
    u_tri <- solve_tridiagonal(x_tri, y_tri, z_tri, w_tri, 2 * N_layers)
    
    # Output shortwave 1 ----
    I_upward_states <- u_tri[seq_len(length(u_tri)) %% 2 != 0]
    I_downward_states <- u_tri[seq_len(length(u_tri)) %% 2 == 0]
    
    
    ### Calculate State Variables ----
    I_states_b <- numeric(N_layers) # Net shortwave direct beam flux per layer
    I_states_d <- numeric(N_layers) # Net shortwave diffuse flux per layer
    I_states <- numeric(N_layers) # Net shortwave flux per layer
    
    I_states_b[i_soil] <- (1 - rho_gb) * SW_Inc_b[k] * T_b[i_soil]
    I_states_d[i_soil] <- (1 - rho_gd) * I_downward_states[i_soil]
    I_states[i_soil] <- I_states_b[i_soil] + I_states_d[i_soil]
    
    for (i in i_bot_l:i_top_l) {
      I_states_d[i] <- (I_downward_states[i] + I_upward_states[i - 1]) * (1 - tau_d[i]) * (1 - omega_l_I) # Equation (14.47)
      I_states_b[i] <- SW_Inc_b[k] * T_b[i] * (1 - tau_b[i]) * (1 - omega_l_I) # Equation (14.48)
      I_states[i] <- I_states_b[i] + I_states_d[i]
    }
    
    # Output shortwave 1 ----
    I_g[k] <- I_states[i_soil] # "Ground Net Flux"
    I_top[k] <- I_states[i_top_l] # "SW Radiation going out of the canopy"
    I_c[k] <- sum(I_states[i_bot_l:i_top_l]) # Equation (14.49) # "Canopy Net Flux"
    
    
    ### Longwave Radiation ----
    
    ### Calculate Tridiagonal Matrix ----
    x_tri <- numeric(2 * N_layers) # Values below the diagonal
    y_tri <- numeric(2 * N_layers) # Values on the diagonal
    z_tri <- numeric(2 * N_layers) # Values above the diagonal
    w_tri <- numeric(2 * N_layers) # Values on the right side of the equation
    
    ### Calculations for the Soil Layer
    i <- i_soil
    
    # Soil Upward Flux - First row of the 14.124 Matrix
    
    x_tri[2 * i - 1] <- 0
    y_tri[2 * i - 1] <- 1
    z_tri[2 * i - 1] <- -rho_g
    w_tri[2 * i - 1] <- eps_g * sigma * T_g[k]^4 # c0 from Eq. (14.127)
    
    # Soil Downward Flux - Second row of the 14.124 Matrix
    
    reflected <- (1 - tau_d[i + 1]) * rho_l_L # left part of equation (14.125)
    transmitted <- (1 - tau_d[i + 1]) * tau_l_L + tau_d[i + 1] # numerator of fraction in right part of equation (14.125)
    a_i <- reflected - (transmitted^2) / reflected # Equation (14.125)
    b_i <- transmitted / reflected # Equation (14.126)
    d_i <- (1 - b_i) * (1 - tau_d[i + 1]) * eps_l * sigma * T_l[k]^4 # Equation (14.128)
    
    x_tri[2 * i + 0] <- -a_i
    y_tri[2 * i + 0] <- 1
    z_tri[2 * i + 0] <- -b_i
    w_tri[2 * i + 0] <- d_i
    
    ### Calculations for the Canopy Layers
    
    for (i in i_bot_l:(i_top_l - 1)) {
      
      # Canopy Layer Upward flux
      
      reflected <- (1 - tau_d[i]) * rho_l_L
      transmitted <- (1 - tau_d[i]) * tau_l_L + tau_d[i]
      f_i <- reflected - (transmitted^2) / reflected # Equation (14.125), same as a_{i-1}
      e_i <- transmitted / reflected # Equation (14.126), same as b_{i-1}
      c_i <- (1 - e_i) * (1 - tau_d[i]) * eps_l * sigma * T_l[k]^4 # Equation (14.127)
      
      x_tri[2 * i - 1] <- -e_i
      y_tri[2 * i - 1] <- 1
      z_tri[2 * i - 1] <- -f_i
      w_tri[2 * i - 1] <- c_i
      
      # Canopy Layer Downward flux
      
      reflected <- (1 - tau_d[i + 1]) * rho_l_L
      transmitted <- (1 - tau_d[i + 1]) * tau_l_L + tau_d[i + 1]
      a_i <- reflected - (transmitted^2) / reflected # Equation (14.125)
      b_i <- transmitted / reflected # Equation (14.126)
      d_i <- (1 - b_i) * (1 - tau_d[i + 1]) * eps_l * sigma * T_l[k]^4 # Equation (14.128)
      
      x_tri[2 * i + 0] <- -a_i
      y_tri[2 * i + 0] <- 1
      z_tri[2 * i + 0] <- -b_i
      w_tri[2 * i + 0] <- d_i
    }
    
    ### Calculations for the top Canopy Layer
    
    i <- i_top_l
    
    # Top Canopy Layer Upward Flux - Second to last row of the 14.124 Matrix
    
    reflected <- (1 - tau_d[i]) * rho_l_L
    transmitted <- (1 - tau_d[i]) * tau_l_L + tau_d[i]
    f_i <- reflected - (transmitted^2) / reflected # Equation (14.125), same as a_{i-1}
    e_i <- transmitted / reflected # Equation (14.126), same as b_{i-1}
    c_i <- (1 - e_i) * (1 - tau_d[i]) * eps_l * sigma * T_l[k]^4 # Equation (14.127)
    
    x_tri[2 * i - 1] <- -e_i
    y_tri[2 * i - 1] <- 1
    z_tri[2 * i - 1] <- -f_i
    w_tri[2 * i - 1] <- c_i
    
    # Top Canopy Layer Downward Flux - Last row of the 14.124 Matrix
    
    x_tri[2 * i + 0] <- 0
    y_tri[2 * i + 0] <- 1
    z_tri[2 * i + 0] <- 0
    w_tri[2 * i + 0] <- LW_Inc[k] # dN from Eq. (14.128)
    
    u_tri <- solve_tridiagonal(x_tri, y_tri, z_tri, w_tri, 2 * N_layers)
    
    ### Calculate State Variables ----
    
    # Output longwave 1 ----
    L_upward_states <- u_tri[seq_len(length(u_tri)) %% 2 != 0]
    L_downward_states <- u_tri[seq_len(length(u_tri)) %% 2 == 0]
    
    #Net longwave flux per layer
    L_states <- numeric(N_layers) 
    
    L_states[i_soil] <-  L_downward_states[i_soil] - L_upward_states[i_soil]
    
    for (i in 2:N_layers){
      L_states[i] <- eps_l*(L_downward_states[i] + L_upward_states[i-1]) * (1-tau_d[i]) - 2*eps_l*sigma*T_l[k]^4*(1-tau_d[i])
    }
    
    # Output longwave 2 ----
    L_g[k] <- L_states[i_soil]  # "Canopy Net Flux"
    L_top[k] <- L_states[i_top_l] # "LW Radiation going out of the canopy"
    L_c[k] <- sum(L_states[i_bot_l:i_top_l]) # "Canopy Net Flux"
    
    
  }
 
  

  # Output ----
  result <- tibble( Timestep = seq(1:timestep),
    I_CNF = I_c, I_GNF = I_g, I_NF = I_c + I_g, I_out = I_top,
    L_CNF = L_c, L_GNF = L_g, L_NF = L_c + L_g, L_out = L_top )
  return(result)  
  
}


## Test ----
mod_output <- norman_model(input_test$SW_Inc_b,
                           input_test$SW_Inc_d, 
                           input_test$LW_Inc,
                           input_test$T_g,
                           input_test$T_l)

mod_output$Timestamp <- input_test$Timestamp
mod_output <- mod_output %>% 
  select(Timestamp, everything())

str(mod_output)


##  plot ----

library(ggpubr)
# make a test data set 
Days <- mod_output %>% 
  filter(day(Timestamp) == "2" |
         day(Timestamp) == "3" | 
         day(Timestamp) == "4") %>% 
  rename(Day = Timestamp)
  

# Shortwave
# Ground net flux
plot_ignf <- Days %>% 
  ggplot(aes(x = Day)) +
  geom_point(aes(y = I_GNF)) +
  geom_line(aes(y = I_GNF)) 

# , color = "brown", show.legend = FALSE

# Canopy net flux
plot_icnf <- Days %>% 
  ggplot(aes(x = Day)) +
  geom_point(aes(y = I_CNF)) +
  geom_line(aes(y = I_CNF)) 

# Net flux
plot_inf <- Days %>% 
  ggplot(aes(x = Day)) +
  geom_point(aes(y = I_NF)) +
  geom_line(aes(y = I_NF)) 

ggarrange(plot_ignf, plot_icnf, plot_inf,
          ncol = 2, nrow = 2) 


# Longwave
# Ground net flux
plot_lgnf <- Days %>% 
  ggplot(aes(x = Day)) +
  geom_point(aes(y = L_GNF)) +
  geom_line(aes(y = L_GNF)) 

# Canopy net flux
plot_lcnf <-Days%>% 
  ggplot(aes(x = Day)) +
  geom_point(aes(y = L_CNF)) +
  geom_line(aes(y = L_CNF)) 

# Net flux
plot_lnf <-Days %>%  
  ggplot(aes(x = Day)) +
  geom_point(aes(y = L_NF)) +
  geom_line(aes(y = L_NF)) 

ggarrange(plot_lgnf, plot_lcnf, plot_lnf,
          ncol = 2, nrow = 2) 




## RMSE ----
  
  # Shortwave
  rmse_I = sqrt(mean((input_test$SW_out - mod_output$I_out)**2))  # W/h

  # Longwave
  rmse_L = sqrt(mean((input_test$LW_out - mod_output$L_out)**2))  # W/h
  

  rmse_I;rmse_L
  
  # check deviation 
  # Shortwave
  dev_I = input_test$SW_out - mod_output$I_out
  
  # Longwave
  dev_L = input_test$LW_out - mod_output$L_out

  deviation <- data.frame(dev_I, dev_L)
  

  # I found out that outgoing radiation in our model is represented only from the top canopy layer.
  # Therefore, our model underestimates the outgoing radiation if there are too many layers.
  # I also noticed that the parameters of the beta function, p and q are important.
  # I changed them so that more leaves are allocated to the upper layer.
  # I think we need to calibrate N layers and canopy profile.
  # By Tatsuro 
  
  
  
  # canopy profile function
  p = 10; q = 4
  canopy_LAI_profile <- function(x){x ^ (p -1) * (1-x)^(q -1)/beta(p, q)}
  curve(canopy_LAI_profile, from = 0, to = 1) 


  




