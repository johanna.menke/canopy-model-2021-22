# to use this file put at the top of your script
# source(here::here("utility/get_lambda.R"))

#' Latent heat of vaporization
#' - according to FAO 56 guidelines lambda = 2.45*10**6 Jkg-1
#' - according to Oncley et al. 2007, Boundary-Layer Meteorol. 123, page 16, 
#' lambda can be dependent on air temperature -> 'Tair' as input needed
#' parameters: (leaf or air) temperature in degC
#' returns: lambd, latent heat of vaporization in J kg-1

get_lambda <- function(temp=NULL){
    if (isnull(temp)){
        lambd = 2.45*10**6
    }
    else{
        lambd = 2.501*10**6 - 2361.*temp  
    }
    return(lambd)
}