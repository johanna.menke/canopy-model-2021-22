# Following script is based on follwoing equations from Bonan (2019):
# page 155, eq. 10.2, 10.5. 10.6
# page 157, eq. 10.12, 10.13, 10.15

# Qa: radiative forcing
# t_leaf: starting leaf temperature (K)
# cp: molar specific heat capacity of air (J mol-1 K-1)
# glw: leaf boundary layer conductance for water vapour
# gbh: leaf boundary layer conductance for heat
# q_a_air: water vapour mol fration (mol mol-1)
# ta: temperature (K)
# p_air: air pressure (hPa)
# emleaf: leaf emissivity (-)


leaf_temp <- function(t_leaf, Qa, cp, glw, gbh, q_a_air, ta, p_air, emleaf){
  
  sigma <- 5.67e-8         # Stefan Boltzman constant (kg s-3 K-4)
  mmh2o <- 18.016 / 1000   # Molecular mass of water vapor (kg mol-1)
  Tfrz <- 273.15           # Freezing point of water (K)
  
  niter <- 0    # Number of iterations
  err <- 1e36   # Energy imbalance (W m-2)
  
  # Iteration is until energy imbalance < 1e-06 W m-2 or to 100 iterations
  
  while (niter<=100 & abs(err)>1e-06){
    
    # Increment iteration counter
    
    # browser()
    
    niter <- niter + 1
    
    # Saturation vapor pressure esat (Pa) and temperature derivative desat (Pa K-1)
    # water vapor mole fraction qsat (mol mol-1)
    
    satvap_output <- get_e_sat(t_leaf)
    esat <- satvap_output[[1]]/100   # transfer the unit Pa -> hPa (same as p_air)
    qsat <- esat / p_air             # Pa to mol fraction
    desat <- satvap_output[[2]]/100
    dqsat <- desat / p_air
  
    #!#  The last codes is:
    #       satvap_output <- get_e_sat(t_leaf)
    #       esat <- satvap_output[[1]]/ p_air
    #       desat <- satvap_output[[2]]/ p_air
    #?# whatever tc= ta or t_leaf, desat=s.output[2] or output/100/p_air
    #?#  the result are similar
     
    
    # Latent heat of vaporization (J mol-1)
    
    lambda <- get_lambda(t_air=t_leaf)   # J kg-1 
    lambda <- lambda * mmh2o             # J kg-1 -> J mol-1
    
    
    # Emitted longwave radiation lwout (W m-2) and temperature derivative dlwout (W m-2 K-1)

    ##AKl## lwout <- 2 * emleaf * sigma * (t_leaf+Tfrz)^4
    ##AKl## dlwout <- -8 * emleaf * sigma * (t_leaf+Tfrz)^3
    lwout <- emleaf * sigma * (t_leaf+Tfrz)^4
    dlwout <- -4 * emleaf * sigma * (t_leaf+Tfrz)^3
    
    
    # Sensible heat flux H (W m-2) and temperature derivative dH (W m-2 K-1)

    ##AKl## H <- 2 * cp * (t_leaf - ta) * gbh
    ##AKl## dH <- -2 * cp * gbh
    H <- cp * (t_leaf - ta) * gbh
    dH <- -1 * cp * gbh
    
    
    # Latent heat flux LE (W m-2) and temperature derivative dLE (W m-2 K-1)

    LE <- lambda * (qsat - q_a_air) * glw
    dLE <- -lambda * dqsat * glw

    
    # Energy imbalance (W m-2)
    
    err <- Qa - lwout - H - LE
    
    
    # Change in leaf temperature 
    
    dtleaf <- -err / (dlwout + dH + dLE)
    
    
    # Update leaf temperature 
    
    t_leaf <- t_leaf + dtleaf
    
    # print(paste0("t_leaf: ", t_leaf, "err: ", err))
  }
  
  return(list(t_leaf, lwout, H, LE))
}